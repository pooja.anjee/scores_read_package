from sqlalchemy import create_engine
import os

_engine_agriculture = None
_engine_financial = None
_engine_analytics = None


def get_engine_agriculture():
    global _engine_agriculture
    if not _engine_agriculture:
        url_agriculture = str(os.environ["DB_URL"]) + '/' + 'agriculture-data'
        _engine_agriculture = create_engine(url_agriculture)
    return _engine_agriculture


def get_engine_financial():

    global _engine_financial
    if not _engine_financial:
        url_financial = str(os.environ["DB_URL"]) + '/' + 'financial-data'
        _engine_financial = create_engine(url_financial)
    return _engine_financial


def get_engine_analytics():

    global _engine_analytics
    if not _engine_analytics:
        url_analytics = str(os.environ["DB_URL"]) + '/' + 'analytics'
        _engine_analytics = create_engine(url_analytics)
    return _engine_analytics


# List of stuff accessible to importers of this module. Just in case
__all__ = ['get_engine_agriculture', 'get_engine_financial',
           'get_engine_analytics']
