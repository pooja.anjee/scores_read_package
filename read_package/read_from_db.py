from datetime import datetime

from read_package import db
import pandas as pd

engine_analytics = db.get_engine_analytics()


def get_agronomic_scores(input_params):
    farms = input_params['farms']
    date = input_params['date']
    # date = datetime.strptime(date, '%Y-%m-%d')
    for ibge_code, crop_area_dict in farms.items():
        agronomic_scores = pd.read_sql_query(sql='SELECT * FROM feature_store.agronomic_scores WHERE ibge_code='
                                            + str(ibge_code), con=engine_analytics)
        print(agronomic_scores)


