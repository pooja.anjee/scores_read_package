import unittest
from datetime import datetime
from main import *
from read_package.read_from_db import get_agronomic_scores


class AgromicScoreTest(unittest.TestCase):
    """
    AgromicScore Test
    """
    input_params = dict(farms={
        5002209: {
            "soybean": 200,
            "winter corn": 100
        },
        2001230: {
            "summer corn": 200
        }
    }, date=datetime(2020, 1, 1), cpf="2332232323", total_short_term_debt=230000, total_long_term_debt=230000)

    def setUp(self):
        print("AgromicScore READ Test initialized !!!")

    def test_read_agronomic_scores(self):
        input_params = self.input_params
        farms = input_params['farms']
        get_agronomic_scores(self.input_params)

if __name__ == '__main__':
    unittest.main()
